#!/usr/bin/bash
set -euo pipefail

# config
SERVIDOR="https://ayaya.beauty"

# validar
(( $# != 1 )) && echo 'error: se necesita al menos un archivo' && exit 1
[[ ! -f "$1" ]] && echo 'error: ese archivo no existe' && exit 1

# extraer nombre del archivo
ARCHIVO="$1"
NOMBRE="$(basename "$ARCHIVO" | tr -cd 'a-zA-Z.')"

# asegurarse de que tenga una extensión
[[ ! "$NOMBRE" == *'.'* ]] && NOMBRE="$NOMBRE.txt"

# subir a $SERVIDOR
< "$ARCHIVO" curl --progress-bar -F "files[]=@-;filename=$NOMBRE" "$SERVIDOR/upload.php?output=text"
