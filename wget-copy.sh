#!/usr/bin/bash

(( $# != 1 )) && echo "error: need one url" && exit 1

url="$1"
reject_files='ttf,exe,zip,tar'

wget \
	--no-verbose \
	--tries=2 \
	--timeout=20 \
	--max-redirect=1 \
	--no-directories \
	--no-host-directories \
	--no-cache \
	--convert-links \
	--page-requisites \
	--reject "$reject_files" \
	-- "$url"
