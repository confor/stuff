#!/usr/bin/bash
REGEX=(
	# documentation
	'(licen[sc]e|readme|change(s|log)|history|security|usage|release.notes)((\-|\.)(es|en|cn))?(\.(md|txt|markdown))?$'
	'(contribut(ors?|ing)|(pull_request|issue)_template|code_of_conduct|authors?|governance|upgrade|guide|testing|roadmap|patre?ons|migrati(on|ng)|faq)(\.(md|txt|markdown))$'

	# developer config files
	'^(\.(travis|zuul|coveralls|gitlab-ci|airtap|istanbul|svgo)|dependabot|funding)\.yml'
	'^\.(npm|eslint|jshint|test|spm|prettier)ignore'
	'^\.(babel|cardinal|eslint|jscs|jshint|lintstaged|prettier|nyc|nvm)rc'
	'\.editorconfig'
	'\.(git?)keep'
	'\.sublime-(project|workspace)'

	# computer-generated files
	# TODO check if .d.ts.map files are needed
	#'\.(m?j|d\.t)s\.map$'

	# typescript definitions
	# '\.d\.ts$'

	# OS trash
	'thumbs\.db'
	'desktop\.ini'
	'\.ds_store'
)

IFS='|'
fd --base-directory node_modules -HItf "(${REGEX[*]})" $*
