#!/usr/bin/bash
set -euo pipefail
curl --silent 'https://en.wikipedia.org/w/api.php?action=parse&format=json&page=Ubuntu%20version%20history&prop=wikitext&disabletoc=1&formatversion=2' | jq -r .parse.wikitext | grep -F == | grep -oE 'Ubuntu ([0-9\.]+)( LTS)? \(\w+ \w+\)'
