#!/usr/bin/bash
set -euo pipefail

device_id="$(xinput --list --short | grep YSPRINGTECH | grep -oP '(?<=id=)(\d+)')"
new_speed=0.3

# Coordinate Transformation Matrix (158):	1, 0, 0, 0, 1, 0, 0, 0, 1

xinput --set-prop $device_id "Coordinate Transformation Matrix" $new_speed 0 0 0 $new_speed 0 0 0 1
