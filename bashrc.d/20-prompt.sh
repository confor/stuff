# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

function nonzero_exit_code() {
    RETVAL=$?
    [ $RETVAL -ne 0 ] && echo " (err $RETVAL)"
}

PS1='${debian_chroot:+($debian_chroot) }\u@\[\e[1;32m\]\H\[\e[m\]:\w$(nonzero_exit_code)\n\$ '
