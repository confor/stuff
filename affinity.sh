#!/usr/bin/bash
set -euo pipefail

if [[ $# -lt 2 ]]; then
	echo 'Error: faltan argumentos'
	echo "Uso: $0 <núcleos> <programa>"
	echo "Ejemplo: $0 0,1,2,3 python calcular.py"
	exit 1
fi

NUCLEOS="$1"
shift

taskset --cpu-list "$NUCLEOS" $*
