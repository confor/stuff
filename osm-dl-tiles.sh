#!/bin/bash

# download tiles from osm
# don't use hotlinks to osm tileservers, its against their terms:
# https://operations.osmfoundation.org/policies/tiles/
# > OpenStreetMap data is free for everyone to use. Our tile servers are not.
# > In particular, downloading an area of over 250 tiles at zoom level 13
# > or higher for offline or later usage is forbidden.

set -euo pipefail

echoerr() {
	1>&2 echo "$@";
}

usage() {
	echo "Usage: $0 -x <horizontal tile index> -y <vertical tile index> -z <zoom level>"
	echo ""
	echo "  Tile indexes must be in numeric format, natural numbers only."
	echo "  The script supports single tiles (-x 100) or a range (-x 100:110)."
	echo "  Don't bulk download hundreds of tiles, read the OSM foundation policies:"
	echo "  https://operations.osmfoundation.org/policies/tiles/"
	echo ""
	echo "  OSM has a naming convention for tile indexes and formulas for"
	echo "  transforming latitude/longitude to file names, see:"
	echo "  https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames"
}

SERVER="https://tile.openstreetmap.org"
CURL_ARGS="--silent --connect-timeout 8 --max-time 60"

## parsing ##
TILE_X=""
TILE_Y=""
TILE_ZOOM=""

while getopts ":hx:y:z:" options; do
	case "$options" in
		h)
			usage
			exit 0
			;;
		x) TILE_X="$OPTARG" ;;
		y) TILE_Y="$OPTARG" ;;
		z) TILE_ZOOM="$OPTARG" ;;
		:)
			echoerr "Error: missing argument for -$OPTARG"
			exit 1
			;;
		*)
			echoerr "Error while parsing arguments"
			exit 1
			;;
	esac
done
## /parsing ##


## validation ##
if [[ -z "$TILE_X" ]] || [[ -z "$TILE_Y" ]]; then
	echoerr "Error: missing tile indexes"
	usage
	exit 1
fi

if [[ -z "$TILE_ZOOM" ]]; then
	echoerr "Error: missing tile zoom level"
	usage
	exit 1
fi

# TODO check if [0-9] changes with user locale
if [[ ! "$TILE_X" =~ ^[0-9]+(:[0-9]+)?$ ]] || [[ ! "$TILE_Y" =~ ^[0-9]+(:[0-9]+)?$ ]]; then
	echoerr "Error: tile index must be numeric (only [0-9] or [0-9]:[0-9])"
	exit 1
fi

if [[ ! "$TILE_ZOOM" =~ ^[0-9]+$ ]]; then
	echoerr "Error: tile zoom must be numeric (0-9)"
	exit 1
fi
## /validation ##


## downloads ##
check_connection() {
	url="$1"
	status=$(curl $CURL_ARGS --head --write-out '%{http_code}' --output /dev/null "$url")
	if [[ "$status" == 000 ]]; then
		echoerr "Error: connection or network error in $url"
		return 1
	fi

	# status ≥ 400
	if [[ "$status" -ge 400 ]]; then
		echoerr "Error: request error, server returned http code $status"
		return 1
	fi

	echo "Server returned $status"
	return 0
}

download_tile() {
	local TILE_ZOOM=$1
	local TILE_X=$2
	local TILE_Y=$3
	echo "  tile_${TILE_ZOOM}_${TILE_X}_${TILE_Y}.png"
	curl $CURL_ARGS --output "tile_${TILE_ZOOM}_${TILE_X}_${TILE_Y}.png" "$SERVER/${TILE_ZOOM}/${TILE_X}/${TILE_Y}.png"
	return $?
}
## /downloads ##

# FIXME: supports only `-x a -y b`, `-x a:b -y c:d` but NOT `-x a:b -y c`
# TODO handle edge case: `-x` range, `-y` single int (or viceversa)
if [[ "$TILE_X" =~ ':' ]] && [[ "$TILE_Y" =~ ':' ]]; then
	TILE_X_START="${TILE_X%:*}"
	TILE_X_END="${TILE_X#*:}"

	TILE_Y_START="${TILE_Y%:*}"
	TILE_Y_END="${TILE_Y#*:}"

	TOTAL_TILES=$(((1+TILE_X_END-TILE_X_START) * (1+TILE_Y_END-TILE_Y_START)))

	echo "Downloading $TOTAL_TILES tiles..."

	THROTTLE=0

	if [[ "$TOTAL_TILES" -gt 100 ]]; then
		echo "Warning: throttling because too many tiles"
		THROTTLE=1
	fi

	i=0
	for ((x=TILE_X_START;x<=TILE_X_END;x++)); do
		for ((y=TILE_Y_START;y<=TILE_Y_END;y++)); do
			if download_tile "$TILE_ZOOM" "$x" "$y"; then
				i=$((i+1))
			else
				echoerr "Warn: error in $x,$y (zoom $TILE_ZOOM)"
			fi

			if [[ "$THROTTLE" -ne 0 ]]; then
				sleep "$THROTTLE"
			fi
		done
	done

	echo "Downloaded $i tiles"
	exit 0

else
	echo "Trying connection against $SERVER"
	if ! check_connection "${SERVER}/${TILE_ZOOM}/${TILE_X}/${TILE_Y}.png"; then
		echoerr "Aborting, network error"
	fi
	echo "Starting download..."

	download_tile "$TILE_ZOOM" "$TILE_X" "$TILE_Y"
	exit 0
fi
