#!/usr/bin/bash
set -euo pipefail

. ./cloudflareenv.sh

_usage() {
	echo "---------------------"
	echo "cloudflare dns script"
	echo "---------------------"
	echo "subcommands:"
	echo "  help          this message"
	echo "  verify        check current api key"
	echo "  zones         list ids of available zones"
	echo "  zone <zone>   show one zone"
	echo "  records <zone>           list all dns records of a zone"
	echo "  record <zone> <record>   show one record from a zone"
	echo ""
	echo "cloudflare zone/record ids are 32 chars long (0-9a-f)"
}

_echoerr() {
	printf "\033[0;31merror:\033[0m %s\n" "$@" >&2
}

_echowarn() {
	printf "\033[0;33mwarn:\033[0m %s\n" "$@" >&2
}

##################
# validate input #
##################
(( $# < 1 )) && _echoerr "missing subcommand, see usage" && exit 1
[[ -z "$EMAIL" ]] && _echoerr "missing email" && exit 1
[[ -z "$APIKEY" ]] && _echoerr "missing api key" && exit 1

#################
# api utilities #
#################
_request() {
	URL="$1"

	curl "https://api.cloudflare.com/client/v4/$URL" \
	  -H "X-Auth-Email: $EMAIL" \
	  -H "Authorization: Bearer $APIKEY" \
	  -H "Content-Type: application/json" \
	  --silent
}

_check_errors() {
	JSON="$1"

	if [[ "$(<<< "$JSON" jq .success)" != "true" ]]; then
		_echoerr "api call unsuccessful"
		_echoerr "returned error:"
		<<< "$JSON" jq '.errors'
		_echoerr "returned messages:"
		<<< "$JSON" jq '.messages'

		return 1
	fi

	return 0
}

#################
# api endpoints #
#################
_verify_key() {
	_request "user/tokens/verify" | jq
}

_list_zones() {
	JSON="$(_request "zones")"
	_check_errors "$JSON" || exit 1

	<<< "$JSON" jq --raw-output '.result[] | [.id, .name] | join(" ")'
}

_zone_details() {
	ID="$1"
	JSON=$(_request "zones/$ID")
	_check_errors "$JSON" || exit 1

	<<< "$JSON" jq '.result'
}

_zone_records() {
	ID="$1"
	JSON=$(_request "zones/$ID/dns_records")
	_check_errors "$JSON" || exit 1

	<<< "$JSON" jq '.result[] | [.id, .type, .name] | join(" ")'
}

_zone_record() {
	ZONE="$1"
	RECORD="$2"
	JSON=$(_request "zones/$ZONE/dns_records/$RECORD")
	_check_errors "$JSON" || exit 1

	<<< "$JSON" jq '.result'
}

###################
# command handler #
###################
case "$1" in
	"help"|"usage"|"--help"|"-h")
		_usage
		;;

	"verify")
		_verify_key
		;;

	"zones")
		_list_zones
		;;

	"zone")
		(( $# < 2 )) && _echoerr "missing argument <zone>" && exit 1
		[[ -z "$2" ]] && _echoerr "missing argument <zone>" && exit 1

		ZONE="$2"
		[[ ${#ZONE} -ne 32 ]] && _echowarn "zone id appears invalid"

		_zone_details "$ZONE"
		;;

	"records")
		(( $# < 2 )) && _echoerr "missing argument <zone>" && exit 1
		[[ -z "$2" ]] && _echoerr "missing argument <zone>" && exit 1

		ZONE="$2"
		[[ ${#ZONE} -ne 32 ]] && _echowarn "zone id appears invalid"

		_zone_records "$ZONE"
		;;

	"record")
		(( $# < 3 )) && _echoerr "usage: record <zone> <record>" && exit 1
		[[ -z "$2" ]] && _echoerr "missing argument <zone>" && exit 1
		[[ -z "$3" ]] && _echoerr "missing argument <record>" && exit 1

		ZONE="$2"
		RECORD="$3"
		[[ ${#ZONE} -ne 32 ]] && _echowarn "zone id appears invalid"
		[[ ${#RECORD} -ne 32 ]] && _echowarn "record id appears invalid"

		_zone_record "$ZONE" "$RECORD"
		;;

	*)
		_echoerr "unknown subcommand. see $0 help"
		exit 1
		;;
esac
