#!/usr/bin/bash
set -eu

source="$(dirname -- "${BASH_SOURCE[0]}")"
target="$HOME/.local/bin"

cd -- "$source"

realname="$(fd -tf -esh | fzf --border --height=10)"
shortname="$realname"

# remove ".sh" from name
if [[ "$realname" == *.sh ]]; then
	shortname="${realname::-3}"
fi

if [[ -f "$target/$shortname" ]]; then
	echo "error: $realname exists in $target/$shortname"
	exit 1
fi

ln -s "$source/$realname" "$target/$shortname"
