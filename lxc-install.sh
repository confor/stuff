#!/usr/bin/bash

set -euo pipefail

export TARGET_USER='debian'
export TARGET_IFACE='eth0'

apt install --assume-yes --no-install-recommends --no-install-suggests \
			lxc libvirt0 libpam-cgfs bridge-utils uidmap lxc-templates

sed -i 's/BRIDGE="true"/BRIDGE="false"/' /etc/default/lxc-net

cat << EOF > /etc/network/interfaces
auto lo
iface lo inet loopback

iface $TARGET_IFACE inet manual

auto br0
iface br0 inet dhcp
	bridge_ports $TARGET_IFACE
	bridge_fd 0
EOF

printf '%s veth br0 10\n' $TARGET_USER > /etc/lxc/lxc-usernet
printf 'lxc.apparmor.profile = unconfined\n' > /etc/lxc/default.conf

# get uid mappings
mappings_u=$(grep -F $TARGET_USER /etc/subuid | tr ':' ' ' | cut -d' ' -f2,3)
mappings_g=$(grep -F $TARGET_USER /etc/subgid | tr ':' ' ' | cut -d' ' -f2,3)

ENSURE_FOLDERS=(
	"/home/$TARGET_USER/.config/"
	"/home/$TARGET_USER/.config/lxc/"
	"/home/$TARGET_USER/.local/"
	"/home/$TARGET_USER/.local/share/"
	"/home/$TARGET_USER/.local/share/lxc/"
)

for folder in "${ENSURE_FOLDERS[@]}"; do
	mkdir -p "$folder"
	chmod a+r "$folder"
	chown $TARGET_USER:$TARGET_USER "$folder"
done

#create default lxc mappings
printf 'lxc.idmap = u 0 %s\nlxc.idmap = g 0 %s\n' "$mappings_u" "$mappings_g" > /home/$TARGET_USER/.config/lxc/default.conf
chown $TARGET_USER:$TARGET_USER /home/$TARGET_USER/.config/lxc/default.conf

loginctl enable-linger $TARGET_USER

echo "reboot"
