#!/usr/bin/bash

set -euo pipefail

echoerr() {
	1>&2 echo "$@";
}

usage() {
	echo "Usage: $0 -q 'search query' -c 'countrycode' -m margins"
	echo ""
	echo "  -q: name of a place in the world"
	echo "  -c: one or more iso 3166-1alpha2 country codes"
	echo "  -m: margin to add to the bounding box (float)"
	echo ""
}

## parsing ##
QUERY=""
COUNTRY=""
MARGIN=""

# https://stackoverflow.com/a/13251774
# > If this option requires an argument, follow the option character by :
# > For example, "f:g" accepts "f" and "g" but "f" requires an additional argument.
while getopts ":hq:c:m:z:" options; do
	case "$options" in
		h)
			usage
			exit 0
			;;
		q) QUERY="$OPTARG" ;;
		c) COUNTRY="$OPTARG" ;;
		m) MARGIN="$OPTARG" ;;
		z) ZOOM="$OPTARG" ;;
		:)
			echoerr "Error: missing argument for -$OPTARG"
			exit 1
			;;
		?)
			echoerr "Error: invalid option -$OPTARG"
			exit 1
			;;
		*)
			echoerr "Error while parsing arguments"
			exit 1
			;;
	esac
done
## /parsing ##

if [[ "$QUERY" = "" ]]; then
	echoerr "Error: missing query"
	exit 1
fi

if [[ "$COUNTRY" = "" ]]; then
	echoerr "Error: missing country code"
	exit 1
fi

if [[ "$MARGIN" = "" ]]; then
	MARGIN=0.0
fi

if [[ "$ZOOM" = "" ]]; then
	echoerr "Error: a zoom level is needed to make tile numbers"
	exit 1
fi

if [[ "$ZOOM" -gt 24 ]]; then
	echoerr "Error: zoom can't go beyond 24"
	exit 1
fi

echo "querying openstreetmap nominatim: '$QUERY' in '$COUNTRY'"

osm_query="$(tr ' ' '+' <<< "$QUERY")"
osm_data=$(curl --silent --output - 'https://nominatim.openstreetmap.org/search?q='"$osm_query"'&format=json&limit=1&countrycodes='"$COUNTRY")

if [[ $(<<< "$osm_data" jq -r '.[0]') = "null" ]]; then
	echo "No results"
	exit 0
fi

echo "found place: '$(<<< "$osm_data" jq -r '.[0].display_name')'"
echo "raw data:"
jq -C '.[0]' <<< "$osm_data"

# Nominatim API returns a boundingbox property of the form:
# south Latitude, north Latitude, west Longitude, east Longitude
osm_bbox_bot=$(<<< "$osm_data" jq --raw-output '.[0].boundingbox[0]')
osm_bbox_top=$(<<< "$osm_data" jq --raw-output '.[0].boundingbox[1]')
osm_bbox_left=$(<<< "$osm_data" jq --raw-output '.[0].boundingbox[2]')
osm_bbox_right=$(<<< "$osm_data" jq --raw-output '.[0].boundingbox[3]')

echo "bounding box: [ $osm_bbox_bot, $osm_bbox_top, $osm_bbox_left, $osm_bbox_right ]"

osm_bbox_bot=$(echo "$osm_bbox_bot - $MARGIN" | bc)
osm_bbox_top=$(echo "$osm_bbox_top + $MARGIN" | bc)
osm_bbox_left=$(echo "$osm_bbox_left - $MARGIN" | bc)
osm_bbox_right=$(echo "$osm_bbox_right + $MARGIN" | bc)

echo "bbox + margin: [ $osm_bbox_bot, $osm_bbox_top, $osm_bbox_left, $osm_bbox_right ]"


# source: https://wiki.openstreetmap.org/w/index.php?title=Slippy_map_tilenames&oldid=2449945#Bourne_shell_with_Awk
function long2tile() {
	echo "$1 $2" | awk '{
		xtile = ($1 + 180.0) / 360 * 2.0^$2;
		xtile += xtile < 0 ? -0.5 : 0.5;
		printf("%d", xtile);
	}'
}

function lat2tile() {
	echo "$1 $2" | awk -v PI=3.14159265358979323846 '{
		tan_x=sin($1 * PI / 180.0)/cos($1 * PI / 180.0);
		ytile = (1 - log(tan_x + 1/cos($1 * PI/ 180))/PI)/2 * 2.0^$2;
		ytile+=ytile<0?-0.5:0.5;
		printf("%d", ytile )
	}';
}

osm_tile_left=$(long2tile "$osm_bbox_left" "$ZOOM")
osm_tile_right=$(long2tile "$osm_bbox_right" "$ZOOM")
osm_tile_top=$(lat2tile "$osm_bbox_top" "$ZOOM")
osm_tile_bot=$(lat2tile "$osm_bbox_bot" "$ZOOM")

echo "bbox tiles horizontal: $osm_tile_left $osm_tile_right"
echo "bbox tiles vertical: $osm_tile_top $osm_tile_bot"
