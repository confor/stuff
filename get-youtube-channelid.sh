#!/bin/bash

set -euo pipefail

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 'https://www.youtube.com/@tag'"
	exit 1
fi

url="$1"

if [[ "$url" = @* ]]; then  # if $1 is a youtube tag
	url="https://www.youtube.com/$url"
fi

if [[ ! "$url" = http* ]]; then
	echo "Error: invalid url"
	exit 1
fi

curl -o- --silent "$url" | htmlq 'meta[itemprop=channelId]' --attribute 'content'
